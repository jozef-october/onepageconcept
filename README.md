# OnePageConcept by Jozef&Jozef

We have developed a plugin that is able to generate OnePagers efficiently and simple. In this section you can find the list of all modules/components developed for the OnePageConcept, together with a user guide for each component how to integrate it within your OctoberCMS layout. 

<br /><br />
## Installation

### OctoberCMS project
If you want to create a new OnePageConcept project, please follow the instructions to setup a new OctoberCMS project first. These instructions can be found [here](https://gitlab.com/jozefjozef/new-OctoberCMS-project).

### Static Pages plugin from Rainlab
First of all, it is important that you have installed the Static [Pages plugin](https://octobercms.com/plugin/rainlab-pages) from Rainlab. That plugin will be used to generate the backend forms. 

### OnePageConcept plugin as Git Submodule
The OnePageConcept plugin is currently only available as git repository, and should be included in your OctoberCMS project as git submodule. The following steps have to be taken.

#### Add git submodule

* Open your terminal and navigate to `your-project-folder/plugins` .
* If it does not consists of a `jozefjozef` folder, create that directory 
```
mkdir jozefjozef
```
* Enter the directory jozefjozef 
```
cd jozefjozef
```
* Now add the git repository as submodule 
```bash
git submodule add git@gitlab.com:jozef-october/onepageconcept.git
```

* If you now give 
```bash
ls 
```
you should see the `onepageconcept` folder.

#### Commit the submodule to the git project
Make sure this module is added to your git project by committing the addition of the submodule.
```bash
git commit -m "Added OnePageConcept submodule to git project"
```
and push the modifications
```
git push -u origin master
```

<br /><br />
## Update OnePageConcept plugin to latest version
Within your OctoberCMS project, navigate to the OnePageConcept plugin folder
```bash
cd your-project-folder/plugins/jozefjozef/onepageconcept
```
and pull the latest version 
```bash
git pull origin master
```
If there were modifications to be applied, you have to commit the submodule version to your git project. This can be done by navigating one level up to the plugins folder
```bash
cd ..
```
If you give 
```bash
pwd
``` 
you should now see `your-project-folder/plugins`. Now add the modifications of the plugin 
```bash
git add onepageconcept
```
and commit the changes. 
<br /><br />
## Get the associated stylesheets
First, login to the backend and create a new theme, e.g. `my-new-theme`. It is recommended to delete the default `themes/demo` folder. 
Now we are going to import all the default stylesheets. These stylesheets are maintained in the [onepageconcept-assets repository](https://gitlab.com/jozef-october/onepageconcept-assets). 

Go to your theme folder
```bash
cd your-project-folder/themes/my-new-theme/
```
and add the submodule 
```bash
git submodule add git@gitlab.com:jozef-october/onepageconcept-assets.git opc-assets
```
Commit the addition of this submodule to your new project by giving
```bash
git commit -m "Added opc-assets submodule to project"
git push origin master
```
If you now give 
```bash
ls
```
you should see it contains the `opc-assets` folder. In that folder, a `scss` directory is located with all relevant stylesheet files. You should not change anything in there. The only thing you have to do, is copying the `settings.example` folder to your themes directory, one level up. If you are in `your-project-folder/themes/my-new-theme` folder, give
```bash
cp -r opc-assets/settings.example settings
```
If you now enter the settings directory you just copied
```bash
cd settings
```
you will see it cointains of `_<module>-settings.scss` files, together with a `_private.scss` file. As the names suggest, the CSS settings for the modules are stored in the `_<module>-settings.scss` files. In `_private.scss` you can include additional custom CSS that should be included for this project specifically. 

You can then generate the resulting (minified / compressed) `style.css` file by giving
```bash
sass opc-assets/scss/style.scss assets/css/style.css --style compressed
```
from within the `your-project/folder/themes/my-new-theme` folder. 

The only thing that remains is adding all changes, commit them, and push all to the git repository on Gitlab.

<br /><br />
## Setup

### Menu 
If you start with your OnePageConcept project, it is recommended to first create a Menu. This can be done in the backend through the OnePageConcept -> Menu page. Create a new menu and provide it with at least one menu item (Home).

### Default layout
A general OnePageConcept layout consists of the following structure in your `master.htm` layout file:

```
description = "OnePageConcept"

[staticPage]
useContent = 1
default = 0

[PageStart]
meta_title_extra = "| VC Toukabri"

[PageEnd]
jquery = 1

[Menu]
menu_id = 1

[Footer]
==
{% component 'PageStart' %}

{variable name="logo" label="Logo" tab="Logo" type="mediafinder" mode="image" }{/variable}
{% component 'Menu' logo_image=logo %}

/* COMPONENTS SHOULD BE PLACED HERE */

/* -------------------------------- */

{% component 'Footer' %}
{% component 'PageEnd' %}
```

The secrion above the `==` signs consists of the layout configuration, the section below the `==` signs lists the components that are used in the layout. From top to bottom, the configuration is as follows:

* Give the description of the layout (arbitrary).
* Include the Static Page plugin `[staticPage]`
* Include the `[PageStart]` component and specify the Meta Title attribute. This will load all required HTML headers, including the style.css stylesheet and meta data. 
* Include the `[PageEnd]` component and specify if jQuery should be included (1 or 0).
* Include the `[Menu]` component and specify the active ID of the menu.
* Include the `[Footer]` component.

The inclusion of these components below the `==` signs is then trivial and does not require any explanation - once you are familiar with OctoberCMS.

#### Additional components
You can then add additional components to your OnePageConcept through the Components section in OctoberCMS. The individual components are listed in the section below, together with a user guide for each component.

<br /><br />
## Built-in features

First, I want to notice you that there are some general features built in.

#### Button
You can generate buttons in almost every richeditor field by the following expression:
```
[button][text]Place your button text here[/text][link]mailto:test@test.nl[/link][color]#ffffff[/color][background_color]#333333[/background_color][/button]
```
This button consists of the following custom fields:

* `[text]...[/text]` the text in between these markers will de displayed as the button text.
* `[link]...[/link]` the address in between these markers will be placed in the href tag.
* `[color]...[/color]` the code in between these markers will be placed in `style="color: ..."`.
* `[background_color]...[/background_color]` the code in between these markers will be placed in `style="background-color: ..."`. 

The button gets the class `jozef-button` which can be styled as wanted in the `_private.scss` file.

<br /><br />

## List of Components 
* [Slider](#slider)
* [Text Picture](#text-picture)
* [Columns](#columns)
* [Google Maps](#google-maps)
* [Contact form](#contact-form)
* [Parallax](#parallax)
* [Roadmap](#roadmap)
* [Footer](#footer)

### Slider
#### Options
These options can be set in the configuration part of the layout file.

```
[Slider mySlider]
fullwidth        = "fullwidth"      // Display component on full width
section_color    = "efefef"         // hexadecimal color code without #
slider           = 1                // Slider ID 
anchor           = "home"           // anchor tag
class            = ''               // any additional css class

==
```
#### Implementation 
```
/* COMPONENTS SHOULD BE PLACED HERE */
|--
|--

{% component 'mySlider' %}

|--
|--
/* -------------------------------- */

```
#### Screenshot
![](screenshots/slider.png)
<br /><br />

### Text Picture
#### Options
These options can be set in the configuration part of the layout file.

```
[ContentTextPicture myContentTextPicture]
fullwidth        = "fullwidth"             // Display component on full width
section_color    = "efefef"                // hexadecimal color code without #
container_color  = "efefef"                // hexadecimal color code without #
font_color       = "333333"                // hexadecimal color code without #
pic_class        = "picture-full"          // picture-full or picture-inline display
order            = "image"                 // order of display: text | image
column_size      = "is-4"                  // bulma column size of the text
padding_settings = "no-padding-bottom"     // no-padding-top | no-padding-bottom | (empty)
vertical_align   = "v-align-bottom"        // v-align-top | v-align-center | v-align-bottom
anchor           = "over-ons"              // anchor tag
class            = ''                      // any additional css class

==
```
#### Implementation 
```
/* COMPONENTS SHOULD BE PLACED HERE */
|--
|--

{variable name="head_data" label="Titel" tab="Text Picture" type="text" }{/variable}
{variable name="content_data" label="Content" tab="Text Picture" type="richeditor" }{/variable}
{variable name="image_data" label="Afbeelding" tab="Text Picture" type="mediafinder" mode="image" }{/variable}

{% component 'TextPicture' head=head_data text=content_data image=image_data %}

|--
|--
/* -------------------------------- */

```
#### Screenshot
![](screenshots/contentTextPicture.png)
<br /><br />

### Columns
#### Options
These options can be set in the configuration part of the layout file.

```
[Columns myColumns]
fullwidth        = "fullwidth"      // Display component on full width
section_color    = "efefef"         // hexadecimal color code without #
container_color  = "efefef"         // hexadecimal color code without #
column_color     = "efefef"         // hexadecimal color code without #
has_icons        = "has_icons"      // instead of background image, display <img></img>
height           = 100              // height of the image if icons
anchor           = "diensten"       // anchor tag
class            = ''               // any additional css class

==
```
#### Implementation 
```
/* COMPONENTS SHOULD BE PLACED HERE */
|--
|--

{repeater name="columns_data" prompt="Add column" tab="Columns" label="Columns"}
	{variable name="image" label="Image" tab="Columns" type="mediafinder" mode="image" }{/variable}
	{variable name="content" label="Content" tab="Columns" type="textarea" }{/variable}
{/repeater}

{% component 'myColumns' columns=columns_data %}

|--
|--
/* -------------------------------- */

```
#### Screenshot
![](screenshots/columns.png)
<br /><br />

### Google Maps
#### Options
These options can be set in the configuration part of the layout file. 

```
[GoogleMaps]
fullwidth        = "fullwidth"      // Display component on full width
section_color    = "efefef"         // hexadecimal color code without #
company          = "Jozef&Jozef"    // company name
street           = "Djept 77"       // Address
zipcode          = "5509LD"
city             = "Veldhoven, Nederland"
zoom             = 12               // zoom level
markerColor      = "ff0000"         // hexadecimal color code without #
API_KEY          = "AIzaSyDXSq4gSNIs-crk_k2mM0aCC6XTJ7aQT5k"
anchor           = "contact"        // anchor tag
class            = ''               // any additional css class

==
```
#### Implementation 
The component `[GoogleMaps]` will load all javascript required to display the GoogleMaps module inside each
```html
<div style="width: 400px; height: 200px;" id="map" class="map-canvas"></div>
```
canvas. So in fact you can place it anywhere. However, a standard component is available:

```
/* COMPONENTS SHOULD BE PLACED HERE */
|--
|--

{% component 'GoogleMaps' %}

|--
|--
/* -------------------------------- */

```
#### Screenshot
![](screenshots/googleMaps.png)

### Contact form
#### Options
These options can be set in the configuration part of the layout file. 

```
[ContactForm myContactForm]
section_color    = "cfd0d1"                    // hexadecimal color code without #
container_color  = "cfd0d1"                    // hexadecimal color code without #
font_color =     = "333333"                    // hexadecimal color code without #
display          = "card"                      // display style ( card | '' )
card_color       = "ffffff"                    // hexadecimal color code without #
to_mail          = "info@jozefenjozef.nl"      // mail is sent to this address
to_name          = "Jozef&Jozef"               // the name belonging to the address
column_size      = "is-7"                      // column size of the form
anchor           = "contact"                   // anchor tag
class            = ''                          // any additional css class

==
```
#### Implementation 

```
/* COMPONENTS SHOULD BE PLACED HERE */
|--
|--

{variable name="info_contact" label="Info" tab="Contact" type="richeditor" }{/variable}
{% component 'myContactForm' info=info_contact %}

|--
|--
/* -------------------------------- */

```
#### Screenshot
![](screenshots/contactForm.png)

### Parallax
#### Options
These options can be set in the configuration part of the layout file. 

```
[Parallax myParallax]
fullwidth    = "fullwidth"      // fullwidth or not (fullwidth | '')
height       = 350              // height of the wrapper
fixed        = "fixed"          // parallax effect (fixed | '')
anchor       = "about-us"       // anchor tag
class        = ''               // any additional css class

==
```
#### Implementation 

```
/* COMPONENTS SHOULD BE PLACED HERE */
|--
|--

{variable name="parallax" label="Image" tab="Parallax" type="mediafinder" mode="image" }{/variable}
{% component 'myParallax' image=parallax %}

|--
|--
/* -------------------------------- */

```
#### Screenshot
![](screenshots/parallax.png)

<br /><br />
### Roadmap
#### Options
These options can be set in the configuration part of the layout file. 

```
[Roadmap myRoadmap]
fullwidth        = "fullwidth"                 // fullwidth or not (fullwidth | '')
section_color    = "cfd0d1"                    // hexadecimal color code without #
container_color  = "cfd0d1"                    // hexadecimal color code without #
font_color       = "333333"                    // hexadecimal color code without #
class            = ''                          // any additional css class
anchor           = "about-us"                  // anchor tag
```

#### Implementation 

```
/* COMPONENTS SHOULD BE PLACED HERE */
|--
|--

{repeater name="roadmap_data" prompt="Add item" tab="Roadmap"}
	{variable name="title" label="Title" tab="Roadmap" type="text" }{/variable}
	{variable name="text" label="Text" tab="Roadmap" type="textarea" }{/variable}
{/repeater}
{% component 'Roadmap' roadmap_data=roadmap_data %}

|--
|--
/* -------------------------------- */

```
#### Screenshot
![](screenshots/roadmap.png)

<br /><br />

### Footer
#### Options
These options can be set in the configuration part of the layout file. 

```
[Footer myFooter]
fullwidth     = "fullwidth"      // fullwidth or not (fullwidth | '')
section_color = "343434"         // hexadecimal color code without #
name          = "VC Toukabri"    // name will be displayed in the footer
fromYear      = 2017             // year will be used for copyright

==
```
#### Implementation 

```
/* COMPONENTS SHOULD BE PLACED HERE */
|--
|--

{% component 'myFooter' %}

|--
|--
/* -------------------------------- */

```
#### Screenshot
![](screenshots/footer.png)

