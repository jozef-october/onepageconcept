<?php namespace JozefJozef\Onepageconcept;

use System\Classes\PluginBase;
use App;
use Event;
use Str;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    	return [
    		'JozefJozef\OnePageConcept\Components\Start' => 'PageStart',
    		'JozefJozef\OnePageConcept\Components\End' => 'PageEnd',
    		'JozefJozef\OnePageConcept\Components\Slider' => 'Slider',
            'JozefJozef\OnePageConcept\Components\Menu' => 'Menu',
            'JozefJozef\OnePageConcept\Components\ContentColumns' => 'Columns',
            'JozefJozef\OnePageConcept\Components\ContentTextPicture' => 'TextPicture',
            'JozefJozef\OnePageConcept\Components\GoogleMaps' => 'GoogleMaps',
            'JozefJozef\OnePageConcept\Components\ContactForm' => 'ContactForm',
            'JozefJozef\OnePageConcept\Components\Parallax' => 'Parallax',
            'JozefJozef\OnePageConcept\Components\Footer' => 'Footer',
            'JozefJozef\OnePageConcept\Components\Roadmap' => 'Roadmap',
            'JozefJozef\OnePageConcept\Components\Listing' => 'Listing',
            'JozefJozef\OnePageConcept\Components\ImageLayer' => 'ImageLayer',
            'JozefJozef\OnePageConcept\Components\Dividor' => 'Dividor',
            'JozefJozef\OnePageConcept\Components\MultiMenu' => 'MultiMenu',
    	];
    }

    public function registerSettings()
    {
    }

    public function registerMarkupTags()
    {
        return [
            'filters' => [
                // A local method, i.e $this->makeTextAllCaps()
                'clean_url' => [$this, 'cleanString'],
                'buttons' => [$this, 'insertButtons']
            ]
        ];
    }

    public function registerMailTemplates()
    {
        return [
            'jozefjozef.onepageconcept::mail.message' => 'Standaard bericht via contactformulier',
        ];
    }

    public function cleanString($string) {
       return Str::slug($string);
    }

    public function boot()
    {
        // Check if we are currently in backend module.
        if (!App::runningInBackend()) {
            return;
        }

        // Listen for `backend.page.beforeDisplay` event and inject js to current controller instance.
        Event::listen('backend.page.beforeDisplay', function($controller, $action, $params) {
            $controller->addCss('/plugins/jozefjozef/onepageconcept/assets/css/additional.css');
        });
    }

    public function get_string_between($string, $start, $end){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    public function insertButtons($text)
    {
        $button = [];

        while($button['raw'] = $this->get_string_between($text, '[button]', '[/button]')){

            $button['text'] = $this->get_string_between($button['raw'], '[text]', '[/text]');
            $button['link'] = $this->get_string_between($button['raw'], '[link]', '[/link]');
            $button['background_color'] = $this->get_string_between($button['raw'], '[background_color]', '[/background_color]');
            $button['color'] = $this->get_string_between($button['raw'], '[color]', '[/color]');

            if( $button['text'] && $button['link'] ){
                $style = '';
                if($button['color']){
                    $style .= 'color: ' . $button['color'] . '; ';
                }
                if($button['background_color']){
                    $style .= 'background-color: ' . $button['background_color'] . ';';
                }

                if(!empty($style)){
                    $style = 'style="' . $style . '"';
                }

                $text = str_replace("[button]" . $button['raw'] . "[/button]", '</p><a class="button-jozef" ' . $style . ' href="' . $button['link'] . '" >' . $button['text'] . '</a><p>', $text);
            }
        }

        $text = str_replace("<p></p>", '', $text);
        
        return $text;
    }
}
