/*
 * Replace all SVG images with inline SVG
 */
 $(document).ready(function(){
  jQuery('.replace-svg').each(function(){
      var $img = jQuery(this);
      var imgID = $img.attr('id');
      var imgClass = $img.attr('class');
      var imgURL = $img.attr('src');

      jQuery.get(imgURL, function(data) {
          // Get the SVG tag, ignore the rest
          var $svg = jQuery(data).find('svg');

          // Add replaced image's ID to the new SVG
          if(typeof imgID !== 'undefined') {
              $svg = $svg.attr('id', imgID);
          }
          // Add replaced image's classes to the new SVG
          if(typeof imgClass !== 'undefined') {
              $svg = $svg.attr('class', imgClass+' replaced-svg');
          }

          // Remove any invalid XML tags as per http://validator.w3.org
          $svg = $svg.removeAttr('xmlns:a');

          // Replace image with new SVG
          $img.replaceWith($svg);

      }, 'xml');

  });

  var el = $(window);

  el.on('scroll', function() {

    if($(document).scrollTop() + window.innerHeight > $('#stop-here').offset().top - 40)
        $('#return-to-top').addClass('is-absolute');
    if($(document).scrollTop() + window.innerHeight < $('#stop-here').offset().top)
      $('#return-to-top').removeClass('is-absolute');

    // get current scroll position
    var currY = el.scrollTop();

      if(currY > 200) {
        $('#return-to-top').fadeIn(200);
      } else {

        $('#return-to-top').fadeOut(200);

      }

  });

  $('#return-to-top').click(function() {      // When arrow is clicked
    $("html, body").animate({ scrollTop: 0 }, "slow");
      return false;
  });
  
});