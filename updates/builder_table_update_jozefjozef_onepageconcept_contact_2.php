<?php namespace JozefJozef\Onepageconcept\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJozefjozefOnepageconceptContact2 extends Migration
{
    public function up()
    {
        Schema::table('jozefjozef_onepageconcept_contact', function($table)
        {
            $table->text('info')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('jozefjozef_onepageconcept_contact', function($table)
        {
            $table->text('info')->nullable(false)->change();
        });
    }
}
