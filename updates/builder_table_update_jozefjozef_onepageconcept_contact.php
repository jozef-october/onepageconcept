<?php namespace JozefJozef\Onepageconcept\Updates;

use DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJozefjozefOnepageconceptContact extends Migration
{
    public function up()
    {
        Schema::table('jozefjozef_onepageconcept_contact', function($table)
        {
            $table->text('comments');
        });
    }
    
    public function down()
    {
        Schema::table('jozefjozef_onepageconcept_contact', function($table)
        {
            $table->dropColumn('comments');
        });

    }
}
