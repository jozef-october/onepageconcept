<?php namespace JozefJozef\Onepageconcept\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJozefjozefOnepageconceptMenus extends Migration
{
    public function up()
    {
        Schema::table('jozefjozef_onepageconcept_menus', function($table)
        {
            $table->text('menu_items');
            $table->increments('id')->unsigned(false)->change();
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
    
    public function down()
    {
        Schema::table('jozefjozef_onepageconcept_menus', function($table)
        {
            $table->dropColumn('menu_items');
            $table->increments('id')->unsigned()->change();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
}
