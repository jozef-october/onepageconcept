<?php namespace JozefJozef\Onepageconcept\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJozefjozefOnepageconceptMenus extends Migration
{
    public function up()
    {
        Schema::create('jozefjozef_onepageconcept_menus', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 128);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jozefjozef_onepageconcept_menus');
    }
}
