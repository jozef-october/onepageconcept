<?php namespace JozefJozef\Onepageconcept\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJozefjozefOnepageconceptContact3 extends Migration
{
    public function up()
    {
        Schema::table('jozefjozef_onepageconcept_contact', function($table)
        {
            $table->string('email', 128);
        });
    }
    
    public function down()
    {
        Schema::table('jozefjozef_onepageconcept_contact', function($table)
        {
            $table->dropColumn('email');
        });
    }
}
