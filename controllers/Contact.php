<?php namespace JozefJozef\Onepageconcept\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Contact extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController'    ];
    
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = [
        'jozefjozef.onepageconcept.contactform' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('JozefJozef.Onepageconcept', 'jozefjozef-onepageconcept', 'jozefjozef-contactform');
    }
}
