<?php namespace JozefJozef\Onepageconcept\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Rijen extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController','Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'jozefjozef.onepageconcept.rows' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('JozefJozef.Onepageconcept', 'jozefjozef-onepageconcept', 'onepageconcept-rijen');
    }
}
