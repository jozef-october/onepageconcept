<?php namespace JozefJozef\Onepageconcept\Models;

use Model;

/**
 * Model
 */
class Slider extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jozefjozef_onepageconcept_sliders';

    public $attachMany = [
        'images' => 'System\Models\File'
    ];
}
