<?php namespace JozefJozef\Onepageconcept\Models;

use Model;

/**
 * Model
 */
class Rows extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jozefjozef_onepageconcept_rows';

    protected $jsonable = ['content'];
}
