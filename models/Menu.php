<?php namespace JozefJozef\Onepageconcept\Models;

use Model;
use Str;

/**
 * Model
 */
class Menu extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jozefjozef_onepageconcept_menus';

    protected $jsonable = ['menu_items'];

    static function getAnchors()
    {
        $menus = Menu::all();
        $options = ['' => 'Geen link'];

        foreach($menus as $menu){
            $items = $menu->menu_items;

            if(is_array($items)){
                foreach($items as $item){
                    $options[Str::slug($item['title'])] = $item['title'];
                }
            }
        }

        return $options;
    }
}
