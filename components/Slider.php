<?php namespace Jozefjozef\OnePageConcept\Components;

use JozefJozef\OnePageConcept\Models\Menu as Menus;
use JozefJozef\OnePageConcept\Models\Slider as Sliders;
use Cms\Classes\ComponentBase;

class Slider extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Slider Component',
            'description' => 'Slideshow'
        ];
    }

    public function defineProperties()
    {
        return [
            'fullwidth' => [
                 'title'             => 'Breedte',
                 'description'       => 'Welke breedte moet de component in beslag nemen?',
                 'type'              => 'dropdown',
                 'options'           => ['' => 'Wrapper', 'fullwidth' => 'Volledige breedte'],
                 'default'           => '',
            ],
            'section_color' => [
                 'title'             => 'Sectie kleur',
                 'description'       => 'Kleur van de brede balk',
                 'default'           => '',
                 'type'              => 'string',
                 'validationPattern' => '^[a-fA-F0-9]+$',
                 'validationMessage' => 'Vul een geldige code in (zonder #)'
            ],
            'slider' => [
                'title'   => 'Slider',
                'type'    => 'dropdown',
                'default' => 1,
            ],

            'anchor' => [
                 'title'             => 'ID anchor',
                 'description'       => 'Vul hier een uniek ID in',
                 'default'           => '',
                 'type'              => 'dropdown',
            ]
        ];
    }

    public function getAnchorOptions()
    {
        return Menus::getAnchors();
    }

    public function onRun()
    {
        $this->addJs('/plugins/jozefjozef/onepageconcept/components/slider/assets/js/ini-slider.js');
    }

    public function onRender()
    {
        // for menu
        $this->page['anchor'] = $this->property('anchor');


        $id_slider = $this->property('slider');
        $slider = Sliders::find($id_slider)->images;
        $jjslider = [];

        foreach($slider as $item){
            if($size = getimagesize($item->getLocalPath())){
                $jjslider[] = [
                    'path' => $item->path,
                    'sizex' => $size[0],
                    'sizey' => $size[1],
                ];
            }
        }

        $this->page['slider'] = $jjslider;
        $this->page['fullwidth'] = $this->property('fullwidth');

    }

    public function getSliderOptions()
    {
        if($sliders = Sliders::orderBy('name')->pluck('name', 'id')->toArray()){
            return $sliders;
        }
    }
}
