<?php namespace JozefJozef\OnePageConcept\Components;

use Cms\Classes\ComponentBase;

class Listing extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Listing Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'fullwidth' => [
                 'title'             => 'Breedte',
                 'description'       => 'Welke breedte moet de component in beslag nemen?',
                 'type'              => 'dropdown',
                 'options'           => ['' => 'Wrapper', 'fullwidth' => 'Volledige breedte'],
                 'default'           => '',
            ],
            'class' => [
                'title'             => 'CSS Class',
                'description'       => 'Deze css class wordt toegevoegd aan de section.',
                'type'              => 'string',
                'default'           => '',
            ],
            'section_color' => [
                 'title'             => 'Sectie kleur',
                 'description'       => 'Kleur van de brede balk',
                 'default'           => '',
                 'type'              => 'string',
                 'validationPattern' => '^[a-fA-F0-9]+$',
                 'validationMessage' => 'Vul een geldige code in (zonder #)'
            ],
            'container_color' => [
                 'title'             => 'Container kleur',
                 'description'       => 'Kleur van de container',
                 'default'           => '',
                 'type'              => 'string',
                 'validationPattern' => '^[a-fA-F0-9]+$',
                 'validationMessage' => 'Vul een geldige code in (zonder #)'
            ],
            'font_color' => [
                 'title'             => 'Font kleur',
                 'description'       => 'Kleur van de tekst',
                 'default'           => '',
                 'type'              => 'string',
                 'validationPattern' => '^[a-fA-F0-9]+$',
                 'validationMessage' => 'Vul een geldige code in (zonder #)'
            ],
            'anchor' => [
                 'title'             => 'ID anchor',
                 'description'       => 'Vul hier een uniek ID in',
                 'default'           => '',
                 'type'              => 'dropdown',
            ],
            'column_1' => [
               'title'             => 'Data',
               'description'       => '',
               'default'           => '',
               'type'              => 'string',
               'showExternalParam' => false
            ],
            'column_2' => [
               'title'             => 'Data',
               'description'       => '',
               'default'           => '',
               'type'              => 'string',
               'showExternalParam' => false
            ],
        ];
    }

    public function getAnchorOptions()
    {
        return Menus::getAnchors();
    }

    public function onRender()
    {
        $this->page['anchor'] = $this->property('anchor');
        $this->page['fullwidth'] = $this->property('fullwidth');
        $this->page['section_color'] = $this->property('section_color');
        $this->page['container_color'] = $this->property('container_color');
        $this->page['font_color'] = $this->property('font_color');
        $this->page['class'] = $this->property('class', '');
        $this->page['column_1'] = $this->property('column_1');
        $this->page['column_2'] = $this->property('column_2');
    }
}
