<?php namespace JozefJozef\OnePageConcept\Components;

use Cms\Classes\ComponentBase;

class Footer extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Footer Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'section_color' => [
                 'title'             => 'Sectie kleur',
                 'description'       => 'Kleur van de brede balk',
                 'default'           => '',
                 'type'              => 'string',
                 'validationPattern' => '^[a-fA-F0-9]+$',
                 'validationMessage' => 'Vul een geldige code in (zonder #)'
            ],
            'container_color' => [
                 'title'             => 'Container kleur',
                 'description'       => 'Kleur van de container',
                 'default'           => '',
                 'type'              => 'string',
                 'validationPattern' => '^[a-fA-F0-9]+$',
                 'validationMessage' => 'Vul een geldige code in (zonder #)'
            ],
            'class' => [
                'title'             => 'CSS Class',
                'description'       => 'Deze css class wordt toegevoegd aan de section.',
                'type'              => 'string',
                'default'           => '',
            ],
            'name' => [
                 'title'             => 'Bedrijfsnaam',
                 'description'       => 'Komt in de footer te staan achter copyright',
                 'default'           => 'Jozef&Jozef',
                 'type'              => 'string',
            ],
            'fromYear' => [
                 'title'             => 'Startjaar',
                 'description'       => 'Vanaf welk jaar is de copyright?',
                 'default'           => 2017,
                 'type'              => 'string',
                 'validationPattern' => '^[0-9]+$',
                 'validationMessage' => 'Dit moet een jaartal zijn'
            ],
            'terms_and_conditions' => [
                 'title'             => 'Algemene voorwaarden',
                 'description'       => 'Link naar de algemene voorwaarden',
                 'default'           => '',
                 'type'              => 'string',
            ],
            'fullwidth' => [
                 'title'             => 'Breedte',
                 'description'       => 'Welke breedte moet de component in beslag nemen?',
                 'type'              => 'dropdown',
                 'options'           => ['' => 'Wrapper', 'fullwidth' => 'Volledige breedte'],
                 'default'           => '',
            ]
        ];
    }

    public function onRender()
    {
        $this->page['name'] = $this->property('name');
        $this->page['terms_and_conditions'] = $this->property('terms_and_conditions');
        $this->page['section_color'] = $this->property('section_color');
        $this->page['container_color'] = $this->property('container_color');
        $this->page['font_color'] = $this->property('font_color');
        $this->page['class'] = $this->property('class', '');

        $fromYear = $this->property('fromYear');
        $thisYear = intval(date('Y'));
        if($thisYear > $fromYear){
            $this->page['year'] = $fromYear . '-' . $thisYear;
        }
        else {
            $this->page['year'] = $thisYear;
        }
    }
}
