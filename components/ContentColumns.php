<?php namespace JozefJozef\OnePageConcept\Components;

use Cms\Classes\ComponentBase;
use JozefJozef\OnePageConcept\Models\Menu as Menus;

class ContentColumns extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Kolommen',
            'description' => 'Content voor text kolommen + afbeeldingen'
        ];
    }

    public function defineProperties()
    {
        return [
            'fullwidth' => [
                 'title'             => 'Breedte',
                 'description'       => 'Welke breedte moet de component in beslag nemen?',
                 'type'              => 'dropdown',
                 'options'           => ['' => 'Wrapper', 'fullwidth' => 'Volledige breedte'],
                 'default'           => '',
            ],
            'class' => [
                'title'             => 'CSS Class',
                'description'       => 'Deze css class wordt toegevoegd aan de section.',
                'type'              => 'string',
                'default'           => '',
            ],
            'section_color' => [
                 'title'             => 'Sectie kleur',
                 'description'       => 'Kleur van de brede balk',
                 'default'           => '',
                 'type'              => 'string',
                 'validationPattern' => '^[a-fA-F0-9]+$',
                 'validationMessage' => 'Vul een geldige code in (zonder #)'
            ],
            'container_color' => [
                 'title'             => 'Container kleur',
                 'description'       => 'Kleur van de brede balk',
                 'default'           => '',
                 'type'              => 'string',
                 'validationPattern' => '^[a-fA-F0-9]+$',
                 'validationMessage' => 'Vul een geldige code in (zonder #)'
            ],
            'column_color' => [
                 'title'             => 'Kolom kleur',
                 'description'       => 'Achtergrond kleur van de kolommen',
                 'default'           => '',
                 'type'              => 'string',
                 'validationPattern' => '^[a-fA-F0-9]+$',
                 'validationMessage' => 'Vul een geldige code in (zonder #)'
            ],
            'has_icons' => [
                'title'             => 'Icoontjes',
                'description'       => 'Toon icoontjes als afbeelding',
                'default'           => '',
                'type'              => 'dropdown',
                'options'           => ['' => 'Nee', 'has-icons' => 'Ja']
            ],
            'height' => [
                'title'             => 'Hoogte van icoontjes',
                'description'       => 'Stel hoogte van de icoontjes (in px)',
                'default'           => '50',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'Vul een geldige waarde in (bijv. 400)',
            ],
            'inner_padding' => [
                'title'             => 'Inner padding',
                'description'       => 'Krijgt anders via css inner padding',
                'default'           => '',
                'type'              => 'dropdown',
                'options'           => ['' => 'Nee', 'inner-padding' => 'Overal', 'inner-padding-text' => 'Alleen tekst']
            ],
            'anchor' => [
                 'title'             => 'ID anchor',
                 'description'       => 'Vul hier een uniek ID in',
                 'default'           => '',
                 'type'              => 'dropdown',
            ],
            'columns' => [
                 'title'             => 'Kolom data',
                 'description'       => 'Leeg laten',
                 'default'           => '',
                 'type'              => 'string',
                 'showExternalParam' => false
            ]
        ];
    }

    public function getAnchorOptions()
    {
        return Menus::getAnchors();
    }

    public function onRender()
    {
        $this->page['anchor'] = $this->property('anchor');
        $this->page['columns'] = $this->property('columns');
        $this->page['section_color'] = $this->property('section_color');
        $this->page['container_color'] = $this->property('container_color');
        $this->page['column_color'] = $this->property('column_color');
        $this->page['inner_padding'] = $this->property('inner_padding');
        $this->page['fullwidth'] = $this->property('fullwidth');
        $this->page['class'] = $this->property('class', '');
        if($this->page['has_icons'] = $this->property('has_icons')){
            $this->page['height'] = $this->property('height');
        }
    }


}
