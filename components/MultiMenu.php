<?php namespace JozefJozef\OnePageConcept\Components;

use Cms\Classes\ComponentBase;
use RainLab\Pages\Components\StaticMenu as StaticMenu;
use Cms\Classes\Theme;
use RainLab\Pages\Classes\Menu as PagesMenu;

class MultiMenu extends StaticMenu
{
    public function componentDetails()
    {
        return [
            'name'        => 'MultiMenu Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'code' => [
                'title'       => 'rainlab.pages::lang.component.static_menu_code_name',
                'description' => 'rainlab.pages::lang.component.static_menu_code_description',
                'type'        => 'dropdown'
            ],
            'section_color' => [
                 'title'             => 'Sectie kleur',
                 'description'       => 'Kleur van de brede balk',
                 'default'           => '',
                 'type'              => 'string',
                 'validationPattern' => '^[a-fA-F0-9]+$',
                 'validationMessage' => 'Vul een geldige code in (zonder #)'
            ],
            'fullwidth' => [
                 'title'             => 'Breedte',
                 'description'       => 'Welke breedte moet de component in beslag nemen?',
                 'type'              => 'dropdown',
                 'options'           => ['' => 'Wrapper', 'fullwidth' => 'Volledige breedte'],
                 'default'           => '',
            ],
            'is_fixed' => [
                 'title'             => 'Fixed?',
                 'description'       => 'Fixed navbar aan bovenkant of onderkant?',
                 'type'              => 'dropdown',
                 'options'           => ['Nee' => '', 'is-fixed-top' => 'Boven', 'is-fixed-bottom' => 'Onder'],
                 'default'           => '',
            ],
            'logo_image' => [
                 'title'             => 'Afbeelding',
                 'description'       => 'Achtergrond afbeelding parallax',
                 'default'           => '',
                 'type'              => 'string',
                 'showExternalParam' => false
            ],
        ];
    }

    public function onRun()
    {
        $this->addJs('/plugins/jozefjozef/onepageconcept/components/multimenu/assets/js/anchor-active.js');
    }

    public function onRender()
    {
        $this->page['section_color'] = $this->property('section_color');

        $this->page['fullwidth'] = $this->property('fullwidth', '');
        $this->page['is_fixed'] = $this->property('is_fixed');

        $this->page['logo_image'] = $this->property('logo_image');

    }

}
