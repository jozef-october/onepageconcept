<?php namespace Jozefjozef\OnePageConcept\Components;

use Cms\Classes\ComponentBase;

class End extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'End Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'jquery' => [
                 'title'             => 'Voeg jQuery toe',
                 'description'       => 'jQuery library wordt ingeladen',
                 'default'           => '1',
                 'type'              => 'dropdown',
            ]
        ];
    }

    public function getJqueryOptions()
    {
        return ['1' => 'Ja', '0' => 'Nee'];
    }

    public function onRun()
    {
        $this->addJs('/plugins/jozefjozef/onepageconcept/assets/js/replace-svg.js');
    }

    public function onRender()
    {
        if($this->property('jquery')){
            $this->page['load_jquery'] = true;
        }
        else {
            $this->page['load_jquery'] = false;
        }
    }
}
