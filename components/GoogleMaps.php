<?php namespace JozefJozef\OnePageConcept\Components;

use Cms\Classes\ComponentBase;
use JozefJozef\OnePageConcept\Models\Menu as Menus;

class GoogleMaps extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'GoogleMaps Component',
            'description' => 'Kaart met locatie van bedrijf'
        ];
    }

    public function defineProperties()
    {
        return [
            'section_color' => [
                 'title'             => 'Sectie kleur',
                 'description'       => 'Kleur van de brede balk',
                 'default'           => '',
                 'type'              => 'string',
                 'validationPattern' => '^[a-fA-F0-9]+$',
                 'validationMessage' => 'Vul een geldige code in (zonder #)'
            ],
            'fullwidth' => [
                 'title'             => 'Breedte',
                 'description'       => 'Welke breedte moet de component in beslag nemen?',
                 'type'              => 'dropdown',
                 'options'           => ['' => 'Wrapper', 'fullwidth' => 'Volledige breedte'],
                 'default'           => '',
            ],
            'company' => [
                 'title'             => 'Bedrijfsnaam',
                 'description'       => '',
                 'default'           => 'Jozef&Jozef',
                 'type'              => 'string',
            ],
            'street' => [
                 'title'             => 'Straat',
                 'description'       => '',
                 'default'           => 'Djept 77',
                 'type'              => 'string',
            ],
            'zipcode' => [
                 'title'             => 'Postcode',
                 'description'       => '',
                 'default'           => '5509LD',
                 'type'              => 'string',
            ],
            'city' => [
                 'title'             => 'Plaats',
                 'description'       => '',
                 'default'           => 'Veldhoven, Nederland',
                 'type'              => 'string',
            ],
            'zoom' => [
                 'title'             => 'Zoom',
                 'description'       => 'Zoomlevel van de kaart',
                 'default'           => 12,
                 'type'              => 'string',
                 'validationPattern' => '^[0-9]+$',
                 'validationMessage' => 'Dit moet een nummer zijn, geen tekst'
            ],
            'markerColor' => [
                 'title'             => 'Marker kleur',
                 'description'       => 'Geef een hexadecimale kleurcode (zonder #)',
                 'default'           => 'ff0000',
                 'type'              => 'string',
                 'validationPattern' => '^[a-fA-F0-9]+$',
                 'validationMessage' => 'Geef een geldige hexadecimale kleurcode zonder # (bijv. ff00ff)'
            ],
            'API_KEY' => [
                 'title'             => 'API Key',
                 'description'       => 'De API key verkregen via Google Maps',
                 'type'              => 'string',
            ], 
            'anchor' => [
                 'title'             => 'ID anchor',
                 'description'       => 'Vul hier een uniek ID in',
                 'default'           => '',
                 'type'              => 'dropdown',
            ]
        ];
    }

    public function onRender()
    {
       $this->page['fullwidth'] = $this->property('fullwidth', '');
       $this->page['section_color'] = $this->property('section_color');
    }

    public function getAnchorOptions()
    {
        return Menus::getAnchors();
    }

    public function onRun()
    {
        $this->page['anchor'] = $this->property('anchor');
        $company = $this->page['company'] = $this->property('company');

        $street = $this->page['street'] = $this->property('street');
        $zipcode = $this->page['zipcode'] = $this->property('zipcode');
        $city = $this->page['city'] = $this->property('city');

        $zoom = $this->page['zoom'] = $this->property('zoom');
        $markerColor = $this->page['markerColor'] = $this->property('markerColor');

       $API_KEY = $this->page['API_KEY'] = $this->property('API_KEY');
       $latlng = $this->page['latlng'] = $this->addressToLatLng($street . ' ' . $zipcode . ' ' . $city, $API_KEY);

       $this->addJs('/plugins/jozefjozef/onepageconcept/components/googlemaps/assets/js/ini-google-maps.php?api=' . $API_KEY . '&googlemaps[lat]=' . $latlng['lat'] . '&googlemaps[lng]=' . $latlng['lng'] . '&googlemaps[zoom]=' . $zoom . '&googlemaps[company]=' . urlencode($company) . '&googlemaps[street]=' . urlencode($street) . '&googlemaps[zipcode]=' . urlencode($zipcode) . '&googlemaps[city]=' . urlencode($city) . '&googlemaps[zoom]=' . $zoom . '&googlemaps[markerColor]=' . $markerColor);

       $this->addJs('https://maps.googleapis.com/maps/api/js?key=' . $API_KEY . '&callback=initMap', ['async' => true, 'defer' => 'true']);
    }

    public function addressToLatLng($address, $API_KEY){

        $furl = "https://maps.google.com/maps/api/geocode/json?address=".$address."&sensor=false&key=" . $API_KEY;
        $url = str_replace(' ', '%20', $furl); 

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $geocode = curl_exec($curl);
        curl_close($curl);

        //$geocode=file_get_contents($url);
        if($geocode !== False) {

            $output= json_decode($geocode);

            $lat = $output->results[0]->geometry->location->lat;
            $lng = $output->results[0]->geometry->location->lng;

            return ["lat" => $lat, "lng" => $lng];
        }
        else { return false; }
    }
}
