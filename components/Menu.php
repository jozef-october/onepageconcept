<?php namespace Jozefjozef\OnePageConcept\Components;

use JozefJozef\OnePageConcept\Models\Menu as Menus;
use Cms\Classes\ComponentBase;

class Menu extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Menu Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'section_color' => [
                 'title'             => 'Sectie kleur',
                 'description'       => 'Kleur van de brede balk',
                 'default'           => '',
                 'type'              => 'string',
                 'validationPattern' => '^[a-fA-F0-9]+$',
                 'validationMessage' => 'Vul een geldige code in (zonder #)'
            ],
            'menu_id' => [
                 'title'             => 'Menu',
                 'description'       => 'Welk menu moet er getoond worden?',
                 'default'           => 1,
                 'type'              => 'dropdown',
            ],
            'fullwidth' => [
                 'title'             => 'Breedte',
                 'description'       => 'Welke breedte moet de component in beslag nemen?',
                 'type'              => 'dropdown',
                 'options'           => ['' => 'Wrapper', 'fullwidth' => 'Volledige breedte'],
                 'default'           => '',
            ],
            'is_fixed' => [
                 'title'             => 'Fixed?',
                 'description'       => 'Fixed navbar aan bovenkant of onderkant?',
                 'type'              => 'dropdown',
                 'options'           => ['Nee' => '', 'is-fixed-top' => 'Boven', 'is-fixed-bottom' => 'Onder'],
                 'default'           => '',
            ],
            'logo_image' => [
                 'title'             => 'Afbeelding',
                 'description'       => 'Achtergrond afbeelding parallax',
                 'default'           => '',
                 'type'              => 'string',
                 'showExternalParam' => false
            ]
        ];
    }

    public function getMenu_idOptions()
    {
        $options = [];

        if($menus = Menus::all()){
            foreach($menus as $item){
                $options[$item->id] = $item->name;
            }

            return $options;
        }
        else {
            return false;
        }
    }

    public function onRun()
    {
        $this->addJs('/plugins/jozefjozef/onepageconcept/components/menu/assets/js/anchor-active.js');
    }

    public function onRender()
    {
        $this->page['section_color'] = $this->property('section_color');

        $this->page['fullwidth'] = $this->property('fullwidth', '');
        $this->page['is_fixed'] = $this->property('is_fixed');

        if($menu = Menus::find($this->property('menu_id'))){
            $this->page['menu_items'] = $menu->menu_items;
        }
        else {
            exit('Menu items zijn niet gedefinieerd');
        }

        $this->page['logo_image'] = $this->property('logo_image');

    }


}
