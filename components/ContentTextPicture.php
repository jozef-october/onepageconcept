<?php namespace JozefJozef\OnePageConcept\Components;

use Cms\Classes\ComponentBase;
use JozefJozef\OnePageConcept\Models\Menu as Menus;

class ContentTextPicture extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'TextPicture Component',
            'description' => 'Een rij met een stukje tekst en een afbeelding'
        ];
    }

    public function defineProperties()
    {
        return [
            'fullwidth' => [
                 'title'             => 'Breedte',
                 'description'       => 'Welke breedte moet de component in beslag nemen?',
                 'type'              => 'dropdown',
                 'options'           => ['' => 'Wrapper', 'fullwidth' => 'Volledige breedte'],
                 'default'           => '',
            ],
            'class' => [
                'title'             => 'CSS Class',
                'description'       => 'Deze css class wordt toegevoegd aan de section.',
                'type'              => 'string',
                'default'           => '',
            ],
            'class_column_text' => [
                'title'             => 'CSS Class column',
                'description'       => 'Deze css class wordt toegevoegd aan de column.',
                'type'              => 'string',
                'default'           => '',
            ],
            'istextarea' => [
                'title'             => 'Format',
                'description'       => 'TextArea or Richeditor?',
                'type'              => 'dropdown',
                'default'           => 0,
                'options'           => [0 => 'Richeditor', 1 => 'TextArea']
            ],
            'section_color' => [
                 'title'             => 'Sectie kleur',
                 'description'       => 'Kleur van de brede balk',
                 'default'           => '',
                 'type'              => 'string',
                 'validationPattern' => '^[a-fA-F0-9]+$',
                 'validationMessage' => 'Vul een geldige code in (zonder #)'
            ],
            'container_color' => [
                 'title'             => 'Container kleur',
                 'description'       => 'Kleur van de container',
                 'default'           => '',
                 'type'              => 'string',
                 'validationPattern' => '^[a-fA-F0-9]+$',
                 'validationMessage' => 'Vul een geldige code in (zonder #)'
            ],
            'font_color' => [
                 'title'             => 'Font kleur',
                 'description'       => 'Kleur van de tekst',
                 'default'           => '',
                 'type'              => 'string',
                 'validationPattern' => '^[a-fA-F0-9]+$',
                 'validationMessage' => 'Vul een geldige code in (zonder #)'
            ],
            'pic_class' => [
                 'title'             => 'Toon afbeelding',
                 'description'       => 'Hoe moet de afbeelding worden weergegeven?',
                 'default'           => 'picture-inline',
                 'type'              => 'dropdown',
                 'options'           => ['picture-inline' => 'Inline', 'picture-full' => 'Volledig']
            ],
            'order' => [
                 'title'             => 'Volgorde',
                 'description'       => 'Afbeelding of tekst eerst?',
                 'default'           => 'text',
                 'type'             => 'dropdown',
                 'options'          => ['text' => 'Tekst', 'image' => 'Afbeelding'],
            ],
            'column_size' => [
                 'title'             => 'Afmeting',
                 'description'       => 'Hoe groot is de kolom van de tekst?',
                 'default'           => 'is-7',
                 'type'              => 'dropdown',
            ],
            'padding_settings' => [
                 'title'             => 'Paddings',
                 'description'       => 'Stel de voorkeur in voor paddings',
                 'default'           => '',
                 'type'              => 'dropdown',
                 'options'           => ['' => 'Boven & Onder', 'no-padding-bottom' => 'Boven', 'no-padding-top' => 'Onder']
            ],
            'vertical_align' => [
                 'title'             => 'Verticale uitlijning',
                 'description'       => 'Hoe moet het uitgelijnd worden?',
                 'default'           => 'v-align-top',
                 'type'              => 'dropdown',
                 'options'           => ['v-align-top' => 'Boven', 'v-align-center' => 'Gecentreerd', 'v-align-bottom' => 'Onder']
            ],
            'anchor' => [
                 'title'             => 'ID anchor',
                 'description'       => 'Vul hier een uniek ID in',
                 'default'           => '',
                 'type'              => 'dropdown',
            ],
            'head' => [
                 'title'             => 'Head',
                 'description'       => 'Koptekst',
                 'default'           => '',
                 'type'              => 'string',
                 'showExternalParam' => false
            ],
            'text' => [
                 'title'             => 'Tekst',
                 'description'       => 'Tekst die wordt weergegeven',
                 'default'           => '',
                 'type'              => 'string',
                 'showExternalParam' => false
            ],
            'image' => [
                 'title'             => 'Afbeelding',
                 'description'       => 'URL van de afbeelding',
                 'default'           => '',
                 'type'              => 'string',
                 'showExternalParam' => false
            ]
        ];
    }

    public function getColumn_sizeOptions()
    {
        $options = [];

        for ($i=1; $i <= 12 ; $i++) {
            $val = 'is-' . $i; 
            $options[$val] = $i . '/12 deel';  
        }

        return $options;
    }

    public function getAnchorOptions()
    {
        return Menus::getAnchors();
    }

    public function onRender()
    {
        $this->page['anchor'] = $this->property('anchor');
        $this->page['pic_class'] = $this->property('pic_class');
        $this->page['order'] = $this->property('order');
        $this->page['column_size'] = $this->property('column_size');
        $this->page['padding_settings'] = $this->property('padding_settings');
        $this->page['text'] = $this->property('text');
        $this->page['image'] = $this->property('image');
        $this->page['head'] = $this->property('head');
        $this->page['vertical_align'] = $this->property('vertical_align');
        $this->page['fullwidth'] = $this->property('fullwidth');
        $this->page['section_color'] = $this->property('section_color');
        $this->page['container_color'] = $this->property('container_color');
        $this->page['font_color'] = $this->property('font_color');
        $this->page['class'] = $this->property('class', '');
        $this->page['istextarea'] = $this->property('istextarea');
        $this->page['class_column_text'] = $this->property('class_column_text', '');
    }
}
