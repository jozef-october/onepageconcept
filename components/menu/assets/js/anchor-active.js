$(document).ready(function(){

  // Cache selectors
  var topMenu = $("#top-menu"),
      topMenuHeight = topMenu.outerHeight()+15,
      // All list items
      menuItems = topMenu.find("a"),
      // Anchors corresponding to menu items
      scrollItems = menuItems.map(function(){
        var selector = $(this).attr("href");
        if(selector.startsWith('#')){
          var item = $(selector);
          if (item.length) { return item; }
        }
      });

  // Bind to scroll
  $(window).scroll(function(){
     // Get container scroll position
     var fromTop = $(this).scrollTop()+topMenuHeight;

     // Get id of current scroll item
     var cur = scrollItems.map(function(){
       if ($(this).offset().top < fromTop)
         return this;
     });
     // Get the id of the current element
     cur = cur[cur.length-1];
     var id = cur && cur.length ? cur[0].id : "";

     // Set/remove active class
     menuItems
       .parent().removeClass("is-active")
       .end().filter("[href='#"+id+"']").parent().addClass("is-active");
  });

  $('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        var el_nav = $("nav.navbar > .container");
        var el_nav_offset = el_nav.outerHeight();
        $('html, body').animate({
          scrollTop: target.offset().top - el_nav_offset
        }, 1000);
      }
    }

    $( "#navbar-menu" ).hide();
    $( "#navbar-icon").removeClass('is-active');
  });

  $( "#navbar-icon" ).click(function() {
    $( "#navbar-menu" ).slideToggle( "slow");
  });
});

