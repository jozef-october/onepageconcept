<?php namespace Jozefjozef\OnePageConcept\Components;

use Cms\Classes\ComponentBase;

class Start extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Start Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'meta_title_extra' => [
                 'title'             => 'Meta title toevoeging',
                 'description'       => 'Dit komt achter de meta title te staan.',
                 'default'           => '| Jozef&Jozef',
                 'type'              => 'string',
            ],
            'navbar_fixed' => [
                'title'             => 'Navbar fixed',
                'description'       => 'Voegt class toe aan HTML tag',
                'default'           => '',
                'type'              => 'dropdown',
                'options'           => ['' => 'Nee', 'has-navbar-fixed-top' => 'Ja']
            ],  
        ];
    }

    public function onRender(){
        $this->page['meta_title_extra'] = $this->property('meta_title_extra');
        $this->page['navbar_fixed'] = $this->property('navbar_fixed');
    }
}
