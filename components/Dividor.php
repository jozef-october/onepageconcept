<?php namespace JozefJozef\OnePageConcept\Components;

use JozefJozef\OnePageConcept\Models\Menu as Menus;
use Cms\Classes\ComponentBase;

class Dividor extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Dividor Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'fullwidth' => [
                 'title'             => 'Breedte',
                 'description'       => 'Welke breedte moet de component in beslag nemen?',
                 'type'              => 'dropdown',
                 'options'           => ['' => 'Wrapper', 'fullwidth' => 'Volledige breedte'],
                 'default'           => '',
            ],
            'class' => [
                'title'             => 'CSS Class',
                'description'       => 'Deze css class wordt toegevoegd aan de section.',
                'type'              => 'string',
                'default'           => '',
            ],
            'section_color' => [
                 'title'             => 'Sectie kleur',
                 'description'       => 'Kleur van de brede balk',
                 'default'           => '',
                 'type'              => 'string',
                 'validationPattern' => '^[a-fA-F0-9]+$',
                 'validationMessage' => 'Vul een geldige code in (zonder #)'
            ],
            'anchor' => [
                 'title'             => 'ID anchor',
                 'description'       => 'Vul hier een uniek ID in',
                 'default'           => '',
                 'type'              => 'dropdown',
            ],
            'image' => [
                 'title'             => 'Afbeelding',
                 'description'       => 'Achtergrond afbeelding parallax',
                 'default'           => '',
                 'type'              => 'string',
                 'showExternalParam' => false
            ],
        ];
    }

    public function getAnchorOptions()
    {
        return Menus::getAnchors();
    }

    public function onRender()
    {
        $this->page['image'] = $this->property('image');
        $this->page['fullwidth'] = $this->property('fullwidth');
        $this->page['class'] = $this->property('class', '');
        $this->page['section_color'] = $this->property('section_color');
        $this->page['anchor'] = $this->property('anchor');
    }
}