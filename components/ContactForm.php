<?php namespace JozefJozef\OnePageConcept\Components;

use Cms\Classes\ComponentBase;
use Validator;
use ValidationException;
use Mail;
use JozefJozef\OnePageConcept\Models\ContactForm as ContactForms;
use JozefJozef\OnePageConcept\Models\Menu as Menus;


class ContactForm extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Contact formulier',
            'description' => 'Standaard contact formulier'
        ];
    }

    public function defineProperties()
    {
        return [
            'class' => [
                'title'             => 'CSS Class',
                'description'       => 'Deze css class wordt toegevoegd aan de section.',
                'type'              => 'string',
                'default'           => '',
            ],
            'section_color' => [
                 'title'             => 'Sectie kleur',
                 'description'       => 'Kleur van de brede balk',
                 'default'           => '',
                 'type'              => 'string',
                 'validationPattern' => '^[a-fA-F0-9]+$',
                 'validationMessage' => 'Vul een geldige code in (zonder #)'
            ],
            'container_color' => [
                 'title'             => 'Container kleur',
                 'description'       => 'Kleur van de container',
                 'default'           => '',
                 'type'              => 'string',
                 'validationPattern' => '^[a-fA-F0-9]+$',
                 'validationMessage' => 'Vul een geldige code in (zonder #)'
            ],
            'font_color' => [
                 'title'             => 'Font kleur',
                 'description'       => 'Kleur van de tekst',
                 'default'           => '',
                 'type'              => 'string',
                 'validationPattern' => '^[a-fA-F0-9]+$',
                 'validationMessage' => 'Vul een geldige code in (zonder #)'
            ],
            'display' => [
                'title'             => 'Weergave',
                'description'       => 'Als kaart of inline?',
                'default'           => 'card',
                'type'              => 'dropdown',
                'options'           => ['card' => 'Kaart', 'inline' => 'inline']
            ],
            'card_color' => [
                 'title'             => 'Kaart kleur',
                 'description'       => 'Achtergrond kleur van contact formulier / bedankt bericht',
                 'default'           => '',
                 'type'              => 'string',
                 'validationPattern' => '^[a-fA-F0-9]+$',
                 'validationMessage' => 'Vul een geldige code in (zonder #)'
            ],
            'to_mail' => [
                 'title'             => 'E-mailadres',
                 'description'       => 'Adres waar het formulier naartoe verstuurd wordt.',
                 'default'           => 'info@jozefenjozef.nl',
                 'type'              => 'string',
            ],
            'to_name' => [
                 'title'             => 'Naam',
                 'description'       => 'Naam van e-mailadres waar het naartoe gestuurd wordt.',
                 'default'           => 'Jozef&Jozef',
                 'type'              => 'string',
            ],
            'column_size' => [
                 'title'             => 'Afmeting',
                 'description'       => 'Hoe groot is de kolom van de tekst?',
                 'default'           => 'is-7',
                 'type'              => 'dropdown',
            ],
            'info' => [
                 'title'             => 'Info',
                 'description'       => 'Informatie (contactgegevens etc.)',
                 'default'           => '',
                 'type'              => 'string',
                 'showExternalParam' => false,
            ],
            'anchor' => [
                 'title'             => 'ID anchor',
                 'description'       => 'Vul hier een uniek ID in',
                 'default'           => '',
                 'type'              => 'dropdown',
            ]
        ];
    }

    public function getAnchorOptions()
    {
        return Menus::getAnchors();
    }

    public function getColumn_sizeOptions()
    {
        $options = [];

        for ($i=1; $i <= 12 ; $i++) {
            $val = 'is-' . $i; 
            $options[$val] = $i . '/12 deel';  
        }

        return $options;
    }

    public function onRender()
    {
        $this->page['anchor'] = $this->property('anchor');
        $this->page['info'] = $this->property('info');
        $this->page['section_color'] = $this->property('section_color');
        $this->page['container_color'] = $this->property('container_color');
        $this->page['font_color'] = $this->property('font_color');
        $this->page['column_size'] = $this->property('column_size');
        $this->page['display'] = $this->property('display');
        $this->page['card_color'] = $this->property('card_color');
        $this->page['class'] = $this->property('class', '');
    }

    public function onRun() {
        $this->addJs('/plugins/jozefjozef/onepageconcept/components/contactform/assets/js/errorhandling.js');
    }



    public function onSubmit() {
        $data = post();

        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'comments' => 'required',
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            throw new ValidationException($validation);
        }
        else {
            $contact = new ContactForms;

            $contact->name = $data['name'];
            $contact->email = $data['email'];
            $contact->comments = $data['comments'];

            $info = [];

            foreach($data as $key => $val){
                if($key != 'name' && $key != 'email' && $key != 'comments' && !empty($val)){
                    $info[ucfirst($key)] = $val;
                }
            }

            $contact->info = $info;

            $extra_info = [];

            $extra_info['to_mail'] = $this->property('to_mail', 'info@jozefenjozef.nl');
            $extra_info['to_name'] = $this->property('to_name', 'Jozef&Jozef');

            $comments = str_replace("|", "\|", $contact->comments);

            $vars = [
                'name' => $contact->name, 
                'email' => $contact->email,
                'comments' => $comments,
                'info' => $contact->info,
            ];

            Mail::send('jozefjozef.onepageconcept::mail.message', $vars, function($message) use ($extra_info) {
                $message->to($extra_info['to_mail'], $extra_info['to_name']);
            });

            $contact->save();
        }
      }
}
