$(document).ready(function(){

  $('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        var el_nav = $("nav.navbar.is-fixed-top > .container");
        var el_nav_offset = el_nav.outerHeight();
        if(!el_nav_offset){
          el_nav_offset = 0;
        }
        $('html, body').animate({
          scrollTop: target.offset().top - el_nav_offset
        }, 1000);
      }
    }

    $( "#navbar-menu" ).hide();
    $( "#navbar-icon").removeClass('is-active');
  });

  $( "#navbar-icon" ).click(function() {
    $( "#navbar-menu" ).slideToggle( "slow");
  });
});

