<?php
  if(isset($_GET['googlemaps']) && $_GET['api']){
    $d = $_GET['googlemaps'];
    $api = $_GET['api'];
?>
var map = [];

function initMap() {
  // Settings

  geocoder = new google.maps.Geocoder();

  var nameBusiness = '<?= urldecode($d['company']); ?>';
  var street = '<?= urldecode($d['street']); ?>';
  var zipcode = '<?= urldecode($d['zipcode']); ?>';
  var city = '<?= urldecode($d['city']); ?>';
  var markerColor = '<?= $d['markerColor']; ?>';

  var address = street + ' , ' + zipcode + ' ' + city;

  geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == 'OK') {

      var contentString = '<div id="info-window">'+
          '<div id="siteNotice">'+
          '</div>'+
          '<h1 id="firstHeading" class="firstHeading">' + nameBusiness + '</h1>'+
          '<div id="bodyContent">'+
          '<p>' + street + '</p>'+
          '<p>' +  zipcode + ' ' + city + '</p>'+
          '</div>'+
          '</div>';

      var infowindow = [];
      var marker = [];
      elems = document.getElementsByClassName('map-canvas');
      for (i = 0; i < elems.length; i++) { 
        map[i].setCenter(results[0].geometry.location);

        marker[i] = new google.maps.Marker({
          map: map[i],
          position: results[0].geometry.location,
          icon: {
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(32, 32),
            url: 'data:image/svg+xml;utf-8, \
              <svg width="100" height="100" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg"> \
                <path style="fill:#' + markerColor + ';" d="M40,0C26.191,0,15,11.194,15,25c0,23.87,25,55,25,55s25-31.13,25-55C65,11.194,53.807,0,40,0z     M40,38.8c-7.457,0-13.5-6.044-13.5-13.5S32.543,11.8,40,11.8c7.455,0,13.5,6.044,13.5,13.5S47.455,38.8,40,38.8z"></path> \
              </svg>',
          }
        });
        infowindow[i] = new google.maps.InfoWindow({
          content: contentString,
          pixelOffset: new google.maps.Size(-23, 0)
        });
        infowindow[i].open(map[i], marker[i]);
      }

    

    } else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });

  
  var latlng = {lat: <?= $d['lat']; ?>, lng: <?= $d['lng']; ?> };
  var mapOptions = {
    zoom: <?= $d['zoom']; ?>,
    center: latlng,
  }
  elems = document.getElementsByClassName('map-canvas');
  for (var i = 0; i < elems.length; i++) {
    map[i] = new google.maps.Map(elems[i],
      mapOptions);
  }

}
<?php
  }
?>